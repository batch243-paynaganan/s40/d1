const mongoose=require('mongoose')
const coursesSchema=new mongoose.Schema({
    name:{type: String, required:[true, "Course is required."]},
    description:{type: String, required: [true, "Course description is required."]},
    price:{type:Number, required: [true, "Price is required."]},
    isActive:{type: Boolean, default: true},
    createdOn:{type: Date, default: new Date()},
    slots:{type: Number, required: [true,"Slot is required"]},
    enrollees:[{
        userId:{
            type: String,
            required:[true, "UserId is required."]
        },
        enrolledOn:{
            type: Date,
            default: new Date()
        }
    }]
})

module.exports=mongoose.model("Courses", coursesSchema)
// Activity
/* 
Create a "User.js" file to store the schema of our users
*/
// models > user.js