const express=require('express')
const router=express.Router()
const courseControllers=require('../controllers/courseControllers')
const auth=require('../auth.js')

router.post('/',auth.verify, courseControllers.addCourse)
router.get('/allActiveCourses', courseControllers.getAllActive) //all active courses
router.get('/:courseId',courseControllers.getCourse) //specific course
router.put('/update/:courseId',auth.verify, courseControllers.updateCourse)
router.patch('/archive/:courseId',auth.verify, courseControllers.archiveCourses)

module.exports=router;