const Courses=require('../models/Courses');
const User=require('../models/User')
const auth=require('../auth.js');

module.exports.addCourse=(req,res)=>{
    const userData=auth.decode(req.headers.authorization)
    let newCourse=new Courses({
        name:req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots
    })
    if(userData.isAdmin){
        newCourse.save()
        .then(result=>{
            console.log(result)
            res.send(true)
        })
        .catch(err=>{
            console.log(err)
            res.send(false)
        })
    }else{
        res.send('Hindi ka admin beh, wag assuming')
    }
}
// retrieve all cactive courses
module.exports.getAllActive=(req,res)=>{
    return Courses.find({isActive:true})
    .then(result=>{
        res.send(result);
    }).catch(err=>{
        res.send(err)
    })
}
// retrieve a specific 
module.exports.getCourse=(req,res)=>{
    const courseId=req.params.courseId;
    return Courses.findById(courseId)
    .then(result=>{
        res.send(result)
    })
    .catch(err=>{
        res.send(err)
    })
}
// update a course
module.exports.updateCourse=(req,res)=>{
    const userData=auth.decode(req.headers.authorization);
    let updatedCourse={
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots
    }
    const courseId=req.params.courseId;
    if(userData.isAdmin){
        return Courses.findByIdAndUpdate(courseId, updatedCourse,{new:true})
        .then(result=>{
            res.send(result)
        })
        .catch(err=>{
            res.send(err)
        })
    }else{
        res.send('wag mo kong maupdate update')
    }
}

module.exports.archiveCourses=(req,res)=>{
    const userData=auth.decode(req.headers.authorization);
    const archiveActiveSlots={isActive:req.body.isActive}
    const courseId=req.params.courseId;
    if(userData.isAdmin){
        return Courses.findByIdAndUpdate(courseId, archiveActiveSlots,{new:true})
        .then(result=>{
            res.send(true)
        })
        .catch(err=>{
            res.send(false)
        })
    }else{
        res.send('Di ka authorized. Asan admin mo?')
    }
}