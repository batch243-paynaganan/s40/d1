// *We require the jsonwebtoken module and then contain it in jwt variable
const jwt=require('jsonwebtoken')
const User = require('./models/User')
const Courses=require('./models/Courses')
//*used in algo for encrypting our data which makes it difficult to decode the information without defined secret key
const secret="CourseBookingAPI"
// *[SECTION] JSON web token
// *Token creation
/*  
    *Analogy:
        *Pack the gift provided with a lock, which can only be opened using the secret code as the key.
*/

module.exports.createToken=(result)=>{
    const data={
        id: result._id,
        email: result.email,
        isAdmin: result.isAdmin
    }
    // *generate a JSON web token using the jwt's sign method
    // *Syntax
    /* 
        *jwt.sign(payload, secretOrPrivateKey,[callbackFunction])
    */
   return jwt.sign(data,secret,{})
}

// *Token Verification
// *Analogy- receive the gift and open the lock to verify if the sender is legitimate and the gift is not tampered with

module.exports.verify=(req,res,next)=>{
    let token=req.headers.authorization;
    console.log(token);
    if(token){
    // *validate the "token" using the verify method, the decrypt the token using the secret code
    /* 
        *Syntax:
            *jwt.verify(token,secret,[callbackFunction])
    */
        token=token.slice(7,token.length);
        return jwt.verify(token, secret, err=>{
            if(err){
                return res.send('Invalid token')
            }else{
                next();
            }
    })
    }else{
        return res.send('No token provided')
}}

// *Token decryption
/* 
*Analogy:
*opening a gift or unwrapping a present
*/
module.exports.decode=(token)=>{
    if(!token){
        return null
    }else{
        token=token.slice(7,token.length)
        return jwt.verify(token,secret,(err,data)=>{
            if(err){
                return null
            }else{
                // *decode method is used to obtain the info from the JWT
                // *Syntax: jwt.decode(token,[options])
                // *it will return an obj with the access to the payload property
                return jwt.decode(token,{complete:true}).payload
            }})
    }
}